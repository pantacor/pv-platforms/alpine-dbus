#!/bin/bash

set -e

dir=`sh -c "cd $(dirname $0); pwd"`
modname=`basename $dir`
GITHEAD=${GITHEAD:-`cat $dir/../.git/modules/$modname/HEAD | sed -e 's/.*heads\///'`}

help_message() {
echo "Usage:"
			echo "mkdocker.sh <operation> [options]"
			echo ""
			echo "  init [-c|-a]  initialize project: create container-ci.conf and dockerfile templates. Use -c or -a for more complex template schemes:"
			echo "    default   <empty> one dockerfile template generates container for several architectures"
			echo "    component -c      default behaviour plus support for different libc implementations"
			echo "    app       -a      default behaviour plus support for different Linux distros"
			echo "  install    install projcet: generate .gitlab-ci.yml and final dockerfiles based on the contents of container-ci.conf"
}

# parse arguments
if [ $# -gt 0 ] && [ $# -lt 3 ] ; then
    op="$1"
	case "$op" in
		("init")
			type="default"
			if [ "$2" == "-c" ]; then type="component"; elif [ "$2" == "-a" ]; then type="app"; fi
			;;
		("install")
			;;
		(*)
			help_message
			exit 1
			;;
	esac
else
	help_message
	exit 0
fi

# execute operations
case "$op" in
	("init")
		# set default parameters
		case "$type" in
			("default")
				jobs=${jobs:-amd64:x86_64::: arm32v7:arm::: arm64v8:aarch64:::}
				;;
			("component")
				jobs=${jobs:-amd64:x86_64:musl:: amd64:x86_64:glibc:: arm32v6:arm:musl:arm-linux-musleabihf: arm32v5:arm:glibc:arm-linux-gnueabi:}
				;;
			("app")
				jobs=${jobs:-amd64:x86_64:::ubuntu arm32v7:arm:::ubuntu}
				;;
		esac
		# write initial configuration
		echo "# Container CI Configuration" > container-ci.conf;
    	declare -p type | cut -d " " -f 3- >> container-ci.conf;
    	declare -p jobs | cut -d " " -f 3- >> container-ci.conf;
		cp container-ci/$type/Dockerfile.template* .
		exit 0
		;;
	("install")
		# load configuration
		if [ -f container-ci.conf ]; then
			source container-ci.conf;
		else
			echo "error: container-ci.conf not found. Initialize project first"
			exit 1
		fi
		# set include yml variable
		case "$type" in
			("default")
				yml_template="build-dockerfile.yml"
				;;
			("component")
				yml_template="component-ci.yml"
				;;
			("app")
				yml_template="app-ci.yml"
				;;
		esac
		# write ci include
		cat > .gitlab-ci.yml << EOF
include:
  remote: https://gitlab.com/pantacor/ci/container-ci/raw/${GITHEAD}/yml/${yml_template}

EOF
		# write ci jobs
		for j in $jobs; do
			# extract variables from config
			arch=`echo $j | sed -e 's/:.*//'`
			qemu_arch=`echo $j | sed -e 's/.*:\(.*\):.*:.*:.*/\1/'`
			libc=`echo $j | sed -e 's/.*:.*:\(.*\):.*:.*/\1/'`
			cross=`echo $j | sed -e 's/.*:.*:.*:\(.*\):.*/\1/'`
			distro=`echo $j | sed -e 's/.*://'`

			dockerfile_name="Dockerfile.template"
		    dockerfile_final="Dockerfile.$arch"
		    job_name=$arch
			# set extend job, dockerfile and job name...
			case "$type" in
				("default")
					extended_job="build-dockerfile"
					;;
				("component")
					extended_job="build-component"
					dockerfile_name=$dockerfile_name.$libc;
					dockerfile_final=$dockerfile_final.$libc;
					job_name=$job_name-$libc;
					if [ "$cross" != "" ]; then dockerfile_name=$dockerfile_name.cross; fi
					;;
				("app")
					extended_job="build-app"
					dockerfile_name=$dockerfile_name.$distro;
			        dockerfile_final=$dockerfile_final.$distro;
			        job_name=$job_name-$distro
					;;
			esac
			# generate final dockerfile
			export ARCH=$arch QEMU_ARCH=$qemu_arch LIBC=$libc CROSS=$cross DISTRO=$distro
			cat $dockerfile_name | envsubst '${ARCH},${QEMU_ARCH},${LIBC},${CROSS},${DISTRO}' > $dockerfile_final

			cat >> .gitlab-ci.yml << EOF2

build-$job_name:
  extends: .$extended_job
  variables:
    ARCH: "$arch"
    QEMU_ARCH: "$qemu_arch"
    LIBC: "$libc"
    CROSS: "$cross"
    DISTRO: "$distro"
EOF2

done
		exit 0
		;;
esac
