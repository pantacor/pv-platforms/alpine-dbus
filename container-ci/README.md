# DOCKER CI TEMPLATE

## Introduction

This template aims to avoid repetitive work when creating a new docker CI. This CI will be in charge of building and maintaining a registry of Pantavisor-installable containers (apps).

## How to use the CI template

To use it, just add this template to your project as a submodule:

```
git submodule add https://gitlab.com/pantacor/ci/container-ci.git
```

After that, you will need to execute our mkdocker.sh script from your newly added submodule, with the init operator and while in the root of your project:

```
./container-ci/mkdocker.sh init
```

This will create a new container-ci.conf file and a dockerfile template. You can now modify container-ci.conf and the dockerfile template to fit your project needs. This configuration file will be generated for the first time if it does not exist in your project. It will define a list of jobs that will look like this:

```
JOBS="amd64:x86_64::: arm32v7:arm:::"
```

Each job is defined following this syntax:

```
<architecture>:<qemu-architecture>:[libc]:[cross-compiler]:[distro]
```

The only mandatory arguments are _architecture_ and _qemu-architecture_. This will generate a job and will substitute the ARCH and QEMU_ARCH variables in the Dockerfile.template.

After modifying container-ci.conf, remember to execute mkdocker.sh with the install option:

```
./container-ci/mkdocker.sh install
```

By default mkdcoker.sh tries to be smart and uses the checked out branch as the remote
tracking url for gitlab-ci.... you can overload this decision through the ```GITHEAD```
environment; e.g.

```
GITHEAD="<ref>" ./container-ci/mkdocker.sh install
```

*IMPORTANT:* Do not forget to commit and push these changes to your GitLab repo so the pipeline can start effectively!

## How to use the output containers

After success, CI will have created a new entry in the GitLab registry. With this you can do the following:

### Test it locally

To test it in your host computer, just run it using docker:

```
docker run -it --rm registry.gitlab.com/pantacor/c-green1/lime-suite:amd64
```

*NOTE:* registry URL may vary depending on your project

### Install it in another dockerfile

To include the binaries in your dockefile:

```
FROM registry.gitlab.com/pantacor/c-green1/lime-suite:amd64-glibc-19-01-container-ci as lime-suite

COPY --chown=0:0 --from=lime-suite /usr/local/ /usr/local/
```

*NOTE:* location may vary depending on your project

### Run it in your Pantavisor device with pvr

To install this container as a Pantavisor app in your device, check our [how to guide](https://docs.pantahub.com/pvr-docker-apps-experience/).

Just remember to use your GitLab registry URL:

```
pvr app add --from registry.gitlab.com/pantacor/c-green1/lime-suite:amd64 lime-suite
```
